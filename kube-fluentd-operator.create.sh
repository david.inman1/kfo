#!/bin/sh -x

CHART_URL='https://github.com/vmware/kube-fluentd-operator/releases/download/v1.8.0/log-router-0.3.0.tgz'

helm install --name kfo ${CHART_URL} \
  --set rbac.create=true \
  --set image.tag=v1.8.0 \
  --debug \
  --set image.repository=jvassev/kube-fluentd-operator


